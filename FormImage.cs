﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TeruSoft.Properties;

namespace TeruSoft
{
    /// <summary>
    /// Form for displaying images
    /// </summary>
    public partial class FormCanvas : Form
    {
        private readonly string imageFilePath;
        private Image sourceImage;
        private Image scaledImage;
       
        public FormCanvas(string ImageFilePath)
        {
            InitializeComponent();
            
            imageFilePath = ImageFilePath;
        }
        
        void FormImageKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
        
        void FormCanvasLoad(object sender, EventArgs e)
        {
            if (File.Exists(imageFilePath))
            {
                // Get screen rectangle
                Rectangle screenRect = Screen.PrimaryScreen.WorkingArea;
            	
               	sourceImage = Image.FromFile(imageFilePath);
            	scaledImage = ScaleImage(sourceImage, (int)(screenRect.Width * 0.6), (int)(screenRect.Height * 0.6));
                
            	// Set the PictureBox image property to this image.
            	pictureBox.Image = scaledImage;
	
                // Adjust form's size accordingly
                ClientSize = scaledImage.Size;
                
                // Position the form at the screen center
                Location = new Point(screenRect.Width / 2  - ClientSize.Width / 2, 
		                             screenRect.Height / 2  - ClientSize.Height / 2);
            }
            else
            {
                MessageBox.Show(Resources.ImageFileNotFoundError, Resources.Error);
                Close();
            }
        }
        
        static Image ScaleImage(Image image, int maxWidth, int maxHeight)
		{
        	if(image.Width < maxWidth && image.Height < maxHeight)
        		return image;
        	
        	var ratioX = (double)maxWidth / image.Width;
		    var ratioY = (double)maxHeight / image.Height;
		    var ratio = Math.Min(ratioX, ratioY);
		
		    var newWidth = (int)(image.Width * ratio);
		    var newHeight = (int)(image.Height * ratio);
		
		    var newImage = new Bitmap(newWidth, newHeight);
		
		    using (var graphics = Graphics.FromImage(newImage))
		        graphics.DrawImage(image, 0, 0, newWidth, newHeight);
		
		    return newImage;
		}
		
		void FormCanvasFormClosed(object sender, FormClosedEventArgs e)
		{
			if(scaledImage != null)
				scaledImage.Dispose();
			
			if(sourceImage != null)
				sourceImage.Dispose();
		}
    }
}
