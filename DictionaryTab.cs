﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TeruSoft.Properties;
using TeruSoft.Utilities;
using TheArtOfDev.HtmlRenderer.WinForms;
using TheArtOfDev.HtmlRenderer.Core.Entities;
using System.Web;

namespace TeruSoft
{
    public partial class DictionaryTab : UserControl
    {
        private static readonly Settings Settings = Settings.Default;
        private readonly Logic Logic;
        private readonly HtmlPanel htmlPanel;

        // Type of this dictionary instance
        public DictionaryType DictType { get; private set; }



        // ######## C O N S T R U C T O R #############
        public DictionaryTab(DictionaryType dictType)
        {
            InitializeComponent();

            DictType = dictType;
            Logic = new Logic(dictType);

            textBoxWarning.Text = Resources.ShitagakiWarning;
            textBoxWarning.ForeColor = Settings.WarningForeColor;
            textBoxWarning.BackColor = Settings.WarningBackColor;

            htmlPanel = new HtmlPanel()
            {
                Text = File.ReadAllText(Path.Combine(Application.StartupPath, "HtmlTemplate.html")),
                Dock = DockStyle.Fill,
                IsContextMenuEnabled = false,
                ContextMenuStrip = contextMenuStrip1
            };

            containerPanel.Controls.Add(htmlPanel);
            htmlPanel.MouseClick += HtmlPanel_OnMouseClick;
            htmlPanel.LinkClicked += HtmlPanel_OnLinkClick;

            switch (dictType)
            {
                case DictionaryType.Kanseiban:
                    DisableWarning();
                    break;

                case DictionaryType.Shitagaki:
                    break;

                case DictionaryType.Canteen:
                    DisableWarning();

                    buttonUncheckAll.Visible = false;
                    buttonUncheckAll.Enabled = false;
                    listViewCodes.Height = buttonShowArticles.Height;

                    int oldTopContainer = containerPanel.Top;
                    containerPanel.Top = buttonShowArticles.Bottom + 5;
                    containerPanel.Height += oldTopContainer - containerPanel.Top;

                    listViewCodes.Enabled = false;
                    break;
            }

            Anchor = (AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left);
            Dock = DockStyle.Fill;
            
            ResetToolStripButtons(false);

            PopulateContextMenu();
        }

        private void PopulateContextMenu()
        {
            string onlineDictsFilePath = Path.Combine(Application.StartupPath, Settings.OnlineDictionariesFileName) + ".txt";

            if (!File.Exists(onlineDictsFilePath))
            {
                MessageBox.Show("Программа не смогла найти файл '" + onlineDictsFilePath
                                + "' (список онлайн словарей).\r\nПроверьте наличие файла.", Resources.Error);
                return;
            }

            string[] dicts = File.ReadAllLines(Path.Combine(Application.StartupPath, Settings.OnlineDictionariesFileName) + ".txt");

            foreach (string element in dicts)
            {
                string[] delimiter = { " - " };
                var res = element.Split(delimiter, 2, StringSplitOptions.None);

                KeyValuePair<string, string> pair = new KeyValuePair<string, string>(res[0], res[1]);

                // check url validity
                if (HelperMethods.IsValidURL(pair.Value))
                {
                    // create new instance of the control
                    ToolStripItem item = new ToolStripMenuItem(pair.Key, HelperMethods.TryGetIcon(pair.Key));

                    // add event handler
                    item.Click += (sender, e) =>
                    {
                        string query = pair.Value.Replace("[KANJI]", htmlPanel.SelectedText.Trim());
                        Process.Start(Uri.EscapeUriString(query));
                    };

                    // add the control to the form
                    searchOnToolStripMenuItem.DropDownItems.Add(item);
                }
                else
                {
                    MessageBox.Show("Ошибка в файле списка онлайн-словарей:\r\n'" + pair.Value
                                    + "' is not a valid URL.\r\nЭтот словарь не будет добавлен.", Resources.Error);
                }
            }
        }

        public bool AddToDict(Article article)
        {
            if (!UniversalDictionary.AddToDict(article)) return false;

            // add codes to listview control (duplicates prevention enabled). Добавить в словарь все, кроме тех, чей код уже имеетсяв словаре
            if (listViewCodes.Items.OfType<ListViewItem>().All(item => item.Text != article.Code))
                listViewCodes.Items.Add(article.Code);

            return true;
        }

        #region GUI ===============================================================
        private void DisableWarning()
        {
            textBoxWarning.Visible = false;
            textBoxWarning.Enabled = false;

            int resizeContainer = containerPanel.Top - textBoxWarning.Top;
            containerPanel.Top = textBoxWarning.Top;
            containerPanel.Height += resizeContainer;
        }

        public int Search(string searchString)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();

            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            List<string> checkedCodes = (from ListViewItem item in listViewCodes.CheckedItems select item.Text).ToList();

            OutputResult result = Logic.OutputArticles(searchString, checkedCodes);

            htmlPanel.SetBody(result.OutputString);

            //watch.Stop();
            //Debug.WriteLine(watch.ElapsedMilliseconds);
            
           ResetToolStripButtons(result.OutputCounter != 0);

            Application.UseWaitCursor = false;
            return result.OutputCounter;
        }

        private void buttonShowArticles_Click(object sender, EventArgs e)
        {
            // TODO: Settings.MaxNumArticlesShown
            Application.UseWaitCursor = true;
            Application.DoEvents();

            List<string> checkedCodes = (from ListViewItem item in listViewCodes.CheckedItems select item.Text).ToList();

            OutputResult result = Logic.OutputArticles(default(string), checkedCodes);

            htmlPanel.SetBody(result.OutputString);
            
            ResetToolStripButtons(result.OutputCounter != 0);
            
            UpdateCounterLabel(result.OutputCounter);

            Application.UseWaitCursor = false;
        }

        private void HtmlPanel_OnLinkClick(object sender, HtmlLinkClickedEventArgs e)
        {
            e.Handled = true;
            
            // (в противном случае главная форма перекрывает диалоговую форму)
            bool alwaysOnTopState = ((FormMain)(FindForm())).TopMost;
            ((FormMain)(FindForm())).TopMost = false;
 
            FormCanvas imageForm = new FormCanvas(HttpUtility.UrlDecode(e.Link));
            imageForm.ShowDialog();

            ((FormMain)(FindForm())).TopMost = alwaysOnTopState;
        }

        private void HtmlPanel_OnMouseClick(object sender, MouseEventArgs e)
        {
        	if (e.Button == MouseButtons.Right)
        		htmlPanel.ContextMenuStrip.Show();
        }

        private void listViewCodes_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            buttonShowArticles.Enabled = (listViewCodes.CheckedItems.Count > 0);
            buttonUncheckAll.Enabled = (listViewCodes.CheckedItems.Count > 0);
            
            //TODO: set focus to FormMain.textBoxSearch
        }

        public void CheckListViewItem(int itemIndex)
        {
            if (listViewCodes.Items.Count > 0)
                listViewCodes.Items[itemIndex].Checked = true;
        }

        private void buttonUncheckAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listViewCodes.CheckedItems)
                item.Checked = false;
        }

        private void ContextMenuStrip1Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string selection = htmlPanel.SelectedText.Trim();

            if (selection == "" || !Regex.IsMatch(selection, Patterns.AnyKanji))
                e.Cancel = true;

            furiganaToolStripMenuItem.Text = "Фуригана для \"" + selection + "\"";
            searchOnToolStripMenuItem.Text = "Искать \"" + selection + "\" на:";
        }

        private void MenuFuriganaClick(object sender, EventArgs e)
        {
            string kanji = htmlPanel.SelectedText.Trim();

            string reading = Logic.GetReadingForKanji(kanji);
            
            MessageBox.Show(reading, "Furigana");
        }

        private void JishoorgToolStripMenuItemClick(object sender, EventArgs e)
        {
            Process.Start(@"http://jisho.org/search/" + htmlPanel.SelectedText.Trim());
        }

        void Button1Click(object sender, EventArgs e)
        {
            Clipboard.Clear();
            Clipboard.SetText(htmlPanel.GetHtml());

            MessageBox.Show(htmlPanel.GetHtml(), "Source code");
        }
        #endregion
        
        #region Sorting
        void ToolStripButtonSortByCodeClick(object sender, EventArgs e)
        {
        	Application.DoEvents();
        	Application.UseWaitCursor = true;
        	
        	OutputResult result = Logic.Sort("Code");
        	
        	htmlPanel.SetBody(result.OutputString);
        	UpdateCounterLabel(result.OutputCounter);
        	
        	foreach(var control in toolStrip1.Items.OfType<ToolStripButton>())
        		control.Enabled = !control.Equals(sender as ToolStripButton);

            Application.UseWaitCursor = false;
        }
        
        void ToolStripButtonSortByRussianClick(object sender, EventArgs e)
        {
        	Application.DoEvents();
        	Application.UseWaitCursor = true;
        	
         	OutputResult result = Logic.Sort("Russian");
        	
        	htmlPanel.SetBody(result.OutputString);
        	UpdateCounterLabel(result.OutputCounter);
        	
        	foreach(var control in toolStrip1.Items.OfType<ToolStripButton>())
        		control.Enabled = !control.Equals(sender as ToolStripButton);
        	
        	Application.UseWaitCursor = false;
        }
        
        void ToolStripButtonSortByEnglishClick(object sender, EventArgs e)
        {
        	Application.DoEvents();
        	Application.UseWaitCursor = true;

        	OutputResult result = Logic.Sort("English");
        	
        	htmlPanel.SetBody(result.OutputString);
        	UpdateCounterLabel(result.OutputCounter);
        	
        	foreach(var control in toolStrip1.Items.OfType<ToolStripButton>())
        		control.Enabled = !control.Equals(sender as ToolStripButton);
        	
        	Application.UseWaitCursor = false;
        }
        
        void ToolStripButtonSortByReadingClick(object sender, EventArgs e)
        {
        	Application.DoEvents();
        	Application.UseWaitCursor = true;
        	
        	OutputResult result = Logic.Sort("Reading");
        	
        	htmlPanel.SetBody(result.OutputString);
        	UpdateCounterLabel(result.OutputCounter);
        	
        	foreach(var control in toolStrip1.Items.OfType<ToolStripButton>())
        		control.Enabled = !control.Equals(sender as ToolStripButton);
        	
        	Application.UseWaitCursor = false;
        }
        #endregion
        
        void ResetToolStripButtons(bool enabled)
        {
        	foreach(var control in toolStrip1.Items.OfType<ToolStripButton>())
        		control.Enabled = enabled;
        }
        
        void UpdateCounterLabel(int numArticles)
        {
        	Label label = ParentForm.Controls.Find("labelCount", false).FirstOrDefault() as Label;
            if (label != null)
                label.Text = string.Format("Показано словарных статей: {0}", numArticles);
        }
    }
}