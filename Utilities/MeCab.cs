﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using NMeCab;

namespace TeruSoft.Utilities
{
    public static class MeCab
    {
        // смещение знака катаканы относительно того же знака хираганы в юникоде
        private const int KatakanaOffset = 96;

        /// <summary>Возвращает коллекцию пар "слово (на кандзи)" - "его чтение"</summary>
        public static IEnumerable<KeyValuePair<string, string>> GetReadings(string inputString)
        {
            // лист, в котором функция вернет пары строк "написание - чтение"
            List<KeyValuePair<string, string>> myList = new List<KeyValuePair<string, string>>();

            MeCabParam param = new MeCabParam { UnkFeature = "UNKNOWN" };
            MeCabTagger tagger = MeCabTagger.Create(param);
            MeCabNode node = tagger.ParseToNode(inputString);

            while (node != null)
            {
                // MeCab の仕様で、実行結果の最初と最後に BOS/EOS というヘッダとフッタのようなオブジェクトが返ってくるらしく、
                // これらには Feature が存在しないので node.Feature を実行するとオブジェクト参照エラーになります。
                // そのため、CharType の値を見て Feature を出力するか判定しています。
                if (node.CharType > 0)
                {
                    // proceed current node only if there are some kanji here (http://www.rikai.com/library/kanjitables/kanji_codes.unicode.shtml)
                    if (Regex.IsMatch(node.Surface, Patterns.AnyKanji))
                    {
                        Debug.WriteLine(node.Surface + "\t" + node.Feature);

                        string yomiHiragana = "";

                        // если это не неизвестное слово
                        if (node.Feature != "UNKNOWN")
                        {
                            // последним с конца идет "произношение", а "фуригана" идет предпоследним (言う: いう vs ゆう)
                            // находим индекс последней и предпоследней запятых
                            int lastIndex = node.Feature.LastIndexOf(",");
                            int secondLastIndex = node.Feature.LastIndexOf(",", lastIndex - 1);

                            // "фуригана" находится между этими запятыми
                            string yomiKatakana = node.Feature.Substring(secondLastIndex + 1, lastIndex - secondLastIndex - 1);

                            // convert katakana reading to hiragana
                            foreach (char katakana in yomiKatakana)
                            {
                                yomiHiragana += (char)(katakana - KatakanaOffset);
                            }
                        }
                        else
                            yomiHiragana = node.Feature;

                        //Debug.WriteLine("     " + node.Surface + ", yomi: " + yomiKatakana + " | " + yomiHiragana);
                        myList.Add(new KeyValuePair<string, string>(node.Surface, yomiHiragana));
                    }
                }
                node = node.Next;
            }
            return myList;
        }
    }
}
