﻿using System;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TeruSoft.Properties;

namespace TeruSoft.Utilities
{
    public static class HelperMethods
    {
        public static bool IsValidURL(string inputString)
        {
            Uri result;

            return Uri.TryCreate(inputString, UriKind.Absolute, out result)
                && (result.Scheme == Uri.UriSchemeHttp
                    || result.Scheme == Uri.UriSchemeHttps);
        }

        public static Image TryGetIcon(string siteName)
        {
            string imagePath = Path.Combine(Application.StartupPath, siteName.ToLower() + ".ico");

            return (File.Exists(imagePath)) ? Image.FromFile(imagePath) : null;
        }

        public static bool SearchStringTooShortWarning(string searchString)
        {
            // поскольку поиск строки с длиной менее 3 символов может занять долгое время, спросить пользователя готов ли он провести такой поиск
            if (searchString.Length < 3 && Regex.IsMatch(searchString, @"[a-zA-zа-яА-я]+"))
            {
                DialogResult result = MessageBox.Show(Resources.SearchStringTooShortWarning,
                                                      Resources.Warning, MessageBoxButtons.YesNo);

                if (result == DialogResult.No)
                    return false;

                return true;
            }

            return true;
        }
    }
}
