﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using TeruSoft.Properties;

namespace TeruSoft.Utilities
{
    public static class FileManager
    {
        public static string MakeLocalCopy(string fileSource, string fileDestination)
        {
            if (!File.Exists(fileSource))
            {
                string fileDir = Path.GetDirectoryName(fileSource);
                string fileName = Path.GetFileName(fileSource);

                MessageBox.Show(string.Format("Файл '{0}' не найден в папке '{1}'. Проверьте наличие файла. " + "\r\n\r\nПрограмма закрывается.", fileName, fileDir), Resources.Error);

                return string.Empty;
            }

            // avoids permission problems on opening the file if it's already opened by someone else
            string localcopyFullPath = Path.Combine(Application.StartupPath, fileDestination);

            File.Copy(fileSource, localcopyFullPath, true);
            return localcopyFullPath;
        }
        
        public static void LogError(Exception ex)
        {
            using (var log = File.AppendText(Path.Combine(Application.StartupPath, "log.txt"))) log.WriteLine(DateTime.Now + " " + ex + "\r\n\r\n");

            if (ex.GetType() == typeof(DataException))
                MessageBox.Show(string.Format("Не удается найти рабочий лист '{0}'.",  Settings.Default.KanseibanWorksheet), Resources.Error);
            else if (ex.GetType() == typeof(FileNotFoundException))
                MessageBox.Show(Resources.FileCopyError, Resources.Error);
            else
                MessageBox.Show(ex.Message, Resources.Error);
        }

        public static void CleanUp()
        {
            string tempFilePath1 = Path.Combine(Application.StartupPath, Settings.Default.KanseibanTempFileName);
            string tempFilePath2 = Path.Combine(Application.StartupPath, Settings.Default.ShitagakiTempFileName);

            if (File.Exists(tempFilePath1))
                File.Delete(tempFilePath1);

            if (File.Exists(tempFilePath2))
                File.Delete(tempFilePath2);
        }

        public static void EmergencyCleanUp(string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);
            Debug.WriteLine("File " + filePath + " deleted");
        }
    }
}
