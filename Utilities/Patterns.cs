﻿namespace TeruSoft.Utilities
{
    public static class Patterns
    {
        // кандзи
        public const string AnyKanji = @"[一-龯]+";   //@"[\u4e00-\u9faf]+";

        // кандзи/хирагана/катакана/ромадзи/латиница
        public const string ValidInputForKanjiColumn = @"[一-龯ぁ-ゖァ-ヷｦ-ﾝＡ-Ｚａ-ｚA-Za-z]+";    //@"[\u4e00-\u9faf\u3040-\u309f\u30a0-\u30ff\uff66-\uff9d\uff21-\uff3a\uff41-\uff5aA-Za-z]+";

        // кандзи/хирагана/катакана/ромадзи
        public const string AnyJapaneseCharacters = @"[一-龯ぁ-ゖァ-ヷｦ-ﾝＡ-Ｚａ-ｚ]+";  //@"[\u4e00-\u9faf\u3040-\u309f\u30a0-\u30ff\uff66-\uff9d\uff21-\uff3a\uff41-\uff5a]+";
    }
}
