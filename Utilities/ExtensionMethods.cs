using System;
using System.Linq;
using System.Web.UI;
using NPOI.SS.UserModel;
using TheArtOfDev.HtmlRenderer.WinForms;

namespace TeruSoft.Utilities
{
    public static class RowExtensions
    {
        public static bool IsEmpty(this IRow row)
        {
            if (row == null)
                return true;

            // we need the Trim() part, because strings like "   " don't evaluate as CellType.Blank
            bool notEmpty = row.Cells.Any(x => x.CellType != CellType.Blank && x.ToString().Trim() != "");

            return !notEmpty;
        }
    }
    
    public static class StringExtensions
    {
    	public static string FirstCharToUpper(this string inputString)
    	{
    		char[] a = inputString.ToCharArray();
        	a[0] = char.ToUpper(a[0]);
        	
        	return new string(a);
    	}
    	
    	/// <summary>
    	/// Call it like this: someString.ConditionalTrim(character => !char.IsLetter(character));
    	/// </summary>
    	public static string ConditionalTrim(this string inputString, Func<char, bool> func)
		{		
			int x = inputString.TakeWhile(c => func(c)).Count();
		
			return inputString.Substring(x);
		}
    }
    
    public static class HtmlRendererExtensions
    {
    	public static void SetBody(this HtmlPanel htmlPanel, string innerHtml)
    	{
    		string startTag = "<body>";
    		string endTag = "</body>";
    		
    		int startIndex = htmlPanel.Text.IndexOf(startTag);
    		int endIndex = htmlPanel.Text.IndexOf(endTag);
    		
    		string textToReplace = htmlPanel.Text.Substring(startIndex + startTag.Length, endIndex - startIndex - endTag.Length);
    		
    		htmlPanel.Text = htmlPanel.Text.Replace(textToReplace, innerHtml);
    	}
    }

    public static class HtmlTextWriterExtensions
    {
        public static void WriteWithTags(this HtmlTextWriter htmlTextWriter, HtmlTextWriterTag htmlTag, string innerText)
        {
            htmlTextWriter.RenderBeginTag(htmlTag);
            htmlTextWriter.Write(innerText);
            htmlTextWriter.RenderEndTag();    
        }

        public static void WriteWithTags(this HtmlTextWriter htmlTextWriter, HtmlTextWriterTag htmlTag, string innerText, HtmlTextWriterAttribute htmlAttribute, string attributeInnerText)
        {
            htmlTextWriter.AddAttribute(htmlAttribute, attributeInnerText);
            htmlTextWriter.RenderBeginTag(htmlTag);
            htmlTextWriter.Write(innerText);
            htmlTextWriter.RenderEndTag();
        }
    }
}