﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using TeruSoft.Properties;
using TeruSoft.Utilities;

namespace TeruSoft
{
    public enum DictionaryType { Kanseiban = 0, Shitagaki = 1, Canteen = 2 }

    public partial class FormMain : Form
    {
        private DictionaryTab currentDictionaryTab;
        private readonly Settings Settings = Settings.Default;

        #region METHODS ============================================
        private bool PopulateTabPages()
        {
            Func<string, string, bool> filterOutCanteenWords = (string1, string2) => string1 == string2;
            Func<string, string, bool> filterOutNonCanteenWords = (string1, string2) => string1 != string2;

            foreach (TabPage tab in tabControl.TabPages)
            {
                DictionaryTab dict = null;

                switch (tab.Name)
                {
                    case "tabPageProduction":
                        dict = new DictionaryTab(DictionaryType.Kanseiban) { Dock = DockStyle.Fill };
                        if (!ProcessExcelFile(dict, filterOutCanteenWords))
                            return false;
                        break;

                    case "tabPageShitagaki":
                        dict = new DictionaryTab(DictionaryType.Shitagaki) { Dock = DockStyle.Fill };
                        if (!ProcessExcelFile(dict))
                            return false;
                        break;

                    case "tabPageCanteen":
                        dict = new DictionaryTab(DictionaryType.Canteen) { Dock = DockStyle.Fill };
                        if (!ProcessExcelFile(dict, filterOutNonCanteenWords))
                            return false;

                        // установить галочку на Canteen в ListView
                        dict.CheckListViewItem(0);

                        break;
                }

                if (dict != null) tab.Controls.Add(dict);
            }

            return true;
        }

        private bool ProcessExcelFile(DictionaryTab dict, params Func<string, string, bool>[] optionalParameters)
        {
            // true if its "Shitagaki", false if it's "Kanseiban" or "Canteen"	   
            bool isShitagaki = dict.DictType == DictionaryType.Shitagaki;

            string currentFolder = Application.StartupPath;
            string excelFilesFolder = Path.Combine(currentFolder, "..");    //".." means one level up. 

            var excelFilePath = !isShitagaki ? Path.Combine(excelFilesFolder, Settings.KanseibanExcelFileName)
                                             : Path.Combine(excelFilesFolder, Settings.ShitagakiExcelFileName);

            var tempFileName = !isShitagaki ? Settings.KanseibanTempFileName : Settings.ShitagakiTempFileName;
            var workSheet = !isShitagaki ? Settings.KanseibanWorksheet : Settings.ShitagakiWorksheet;
            var codeColumn = !isShitagaki ? Settings.KanseibanCodeColumn : Settings.ShitagakiCodeColumn;
            var kanjiColumn = !isShitagaki ? Settings.KanseibanKanjiColumn : Settings.ShitagakiKanjiColumn;
            var readingColumn = !isShitagaki ? Settings.KanseibanReadingColumn : Settings.ShitagakiReadingColumn;
            var russianColumn = !isShitagaki ? Settings.KanseibanRussianColumn : Settings.ShitagakiRussianColumn;
            var englishColumn = !isShitagaki ? Settings.KanseibanEnglishColumn : Settings.ShitagakiEnglishColumn;
            var commentColumn = !isShitagaki ? Settings.KanseibanCommentColumn : Settings.ShitagakiCommentColumn;
            var imageColumn = !isShitagaki ? Settings.KanseibanImageColumn : Settings.ShitagakiImageColumn;



            // copy file to application's directory (avoids permission problems on opening the file if it's already opened by someone else)
            string localcopyFullPath = FileManager.MakeLocalCopy(excelFilePath, tempFileName + DateTime.Now.Ticks + ".xls");



            if (localcopyFullPath == string.Empty)
                return false;

            try
            {
                HSSFWorkbook hssfwb;
                using (FileStream file = new FileStream(localcopyFullPath, FileMode.Open, FileAccess.Read))
                {
                    hssfwb = new HSSFWorkbook(file);
                }
                ISheet sheet = hssfwb.GetSheet(workSheet);



                // Find header row
                int? headerRowIndex = GetHeaderRowIndex(sheet, codeColumn);

                // Quit if no header row was found
                if (headerRowIndex == null) return false;



                // dict to store column names and indices 
                Dictionary<int, string> dictHeaders = new Dictionary<int, string>();
                // dict полей класса Article (Key) и соответствующих им названий колонок в файле (Value) 
                Dictionary<string, string> dictColumnNamesToPropertyNames = new Dictionary<string, string>()
                {
                    {"Code",        codeColumn },
                    {"Kanji",       kanjiColumn},
                    {"Reading",     readingColumn},
                    {"Russian",     russianColumn},
                    {"English",     englishColumn},
                    {"Comment",     commentColumn},
                    {"ImagePath",   imageColumn}
                };



                // fill 'headers' dict with column names and corresponding indices
                IRow headerRow = sheet.GetRow((int)headerRowIndex);
                foreach (ICell cell in headerRow)
                {
                    if (cell.ToString().Trim() == "") continue;

                    foreach (KeyValuePair<string, string> columnToProperty in dictColumnNamesToPropertyNames)
                    {
                        if (cell.ToString() == columnToProperty.Value)
                            dictHeaders.Add(cell.ColumnIndex, columnToProperty.Key);
                    }
                }



                // loop through non-empty rows of the spreadsheet (starting one row beneath the header) 
                // and add them to the dict as articles
                for (int r = (int)headerRowIndex + 1; r <= sheet.LastRowNum; r++)
                {
                    IRow currentRow = sheet.GetRow(r);

                    // we skip empty rows and process only those which are not empty
                    if (currentRow.IsEmpty()) continue;



                    if (optionalParameters.Length > 0)
                    {
                        Func<string, string, bool> method = optionalParameters[0];


                        string codeColumnName = dictColumnNamesToPropertyNames
                                                .FirstOrDefault(x => x.Value == codeColumn).Key;
                        int codeColumnIndex = dictHeaders
                                                .FirstOrDefault(x => x.Value == codeColumnName).Key;


                        bool filterOutConditionSatisfied = method(currentRow.Cells[codeColumnIndex].ToString(), "Canteen");

                        if (filterOutConditionSatisfied) continue;
                    }



                    Article article = new Article(dict.DictType);

                    foreach (ICell currentCell in currentRow)
                    {
                        // skip empty cells
                        if (currentCell == null) continue;

                        string propertyName;
                        dictHeaders.TryGetValue(currentCell.ColumnIndex, out propertyName);

                        // пропускаем ячейки в колонках, для которых в классе Article нет соответствующих свойств
                        if (propertyName == null) continue;

                        // переменная с типом "Article"
                        Type type = article.GetType();

                        // ссылка на свойство с именем "propertyName" типа "type" 
                        PropertyInfo propertyInfo = type.GetProperty(propertyName);

                        if (propertyName == "ImagePath" && currentCell.Hyperlink != null)
                            propertyInfo.SetValue(article, RelativeToAbsolute(currentCell.Hyperlink.Address), null);
                        else
                            propertyInfo.SetValue(article, currentCell.ToString(), null);
                    }

                    // add to dictionary
                    dict.AddToDict(article);
                }

                // Debug.WriteLine("File " + localcopyFullPath + " processed");

                return true;
            }
            catch (Exception ex)
            {
                FileManager.LogError(ex);
                throw;
            }
            finally
            {
                FileManager.EmergencyCleanUp(localcopyFullPath);
            }
        }

        private string RelativeToAbsolute(string path)
        {
            string result = Path.IsPathRooted(path)
                ? path
                : Path.Combine(Path.Combine(Application.StartupPath, ".."), path);

            return result;
        }

        private static int? GetHeaderRowIndex(ISheet sheet, string columnToSearchBy)
        {
            int? headerRowIndex = null;

            for (int r = 0; r <= sheet.LastRowNum; r++)
            {
                IRow currentRow = sheet.GetRow(r);

                // we skip empty rows and process only those which are not empty
                if (!currentRow.IsEmpty())
                {
                    ICell cell = currentRow.Cells.FirstOrDefault(x => x.ToString() == columnToSearchBy);

                    if (cell != null)
                    {
                        headerRowIndex = currentRow.RowNum;
                        break;
                    }
                }
            }

            return headerRowIndex;
        }
        #endregion



        #region GUI ================================================
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            string searchWord = textBoxSearch.Text;
            if (string.IsNullOrEmpty(searchWord)) return;

            int articlesFound = currentDictionaryTab.Search(searchWord.Trim());

            labelCount.Text = string.Format("Найдено словарных статей: {0}", articlesFound);
        }

        private void checkBoxAlwaysOnTop_CheckedChanged(object sender, EventArgs e)
        {
            TopMost = ((CheckBox)sender).Checked;
        }

        private void textBoxSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!buttonSearch.Enabled) return;

            if (e.KeyChar == (char)Keys.Return)
                buttonSearch.PerformClick();
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            buttonSearch.Enabled = !string.IsNullOrEmpty(textBoxSearch.Text);
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var casted = tabControl.SelectedTab.Controls.Cast<Control>().First(x => x is DictionaryTab);
            currentDictionaryTab = (DictionaryTab)casted;

            textBoxSearch.Focus();
        }
        #endregion GUI



        #region FORM ===============================================
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            if (!PopulateTabPages())
                Close();
            else
            {
                Control control = tabControl.SelectedTab.Controls.Cast<Control>().FirstOrDefault(x => x is DictionaryTab);

                if (control != null)
                    currentDictionaryTab = (DictionaryTab)control;

                toolStripStatusLabel1.Text = Settings.StatusStripText;
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            FileManager.CleanUp();
        }
        #endregion FORM
    }
}