﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TeruSoft.Utilities;

namespace TeruSoft
{
    public static class UniversalDictionary
    {
        public static List<Article> ListArticles = new List<Article>();

        public static bool AddToDict(Article article)
        {
            // Пропустить (не заносить) в словарь, если в колонке "Kanji" недопустимые символы
            if (!Regex.IsMatch(article.Kanji, Patterns.ValidInputForKanjiColumn))
                return false;

            // (если же все ок,) добавить в словарь
            ListArticles.Add(article);
            return true;
        }
    }
}
