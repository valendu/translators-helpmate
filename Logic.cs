﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TeruSoft.Properties;
using TeruSoft.Utilities;

namespace TeruSoft
{
    public struct OutputResult
    {
        public string OutputString { get; set; }
        public int OutputCounter { get; set; }
    }

    public class Logic
    {
        private readonly Output output = new Output();
        private readonly DictionaryType DictType;

        private List<Article> lastQueryResult;
        private string lastSearchString;

        // TODO: NumArticlesDisplayed property. To be able to toogle label counter on tab_change

        // ==========================================================================================================================================

        public Logic(DictionaryType dictType)
        {
            DictType = dictType;
        }

        public OutputResult OutputArticles(string searchString, List<string> searchCodes)
        {
            output.Clear();

            lastQueryResult = new List<Article>();
            lastSearchString = searchString;

            // loop through all articles in the dict
            foreach (Article article in UniversalDictionary.ListArticles.Where(article => article.SourceDictionary == DictType)
                                                                        .Where(article => searchCodes.Count <= 0 || searchCodes.Contains(article.Code)))
            {
                if (searchString != default(string))
                    if (!HasMatches(article, searchString))
                        continue;

                lastQueryResult.Add(article);

                // Вывести №, кандзи, чтение, английский, русский (в нужном шрифте и цвете)
                output.ProcessHtml(article, searchString);
            }

            return output.ArticlesDisplayed == 0
                ? new OutputResult { OutputString = Resources.NoMatchesFound, OutputCounter = output.ArticlesDisplayed }
                : new OutputResult { OutputString = output.ToString(), OutputCounter = output.ArticlesDisplayed };
        }

        public OutputResult Sort(string property)
        {
            //TODO show and sort by code - DRY complianct??

            output.Clear();

            IOrderedEnumerable<Article> conditionFalseGroup = null;
            IOrderedEnumerable<IGrouping<string, Article>> conditionTrueGroup = null;

            // TODO toggle sort buttons enabled-disabled state according to last query results
            if (lastQueryResult == null)
                return new OutputResult { OutputString = "Нечего сортировать", OutputCounter = 0 };

            Func<char, bool> condition;

            switch (property)
            {
                case "Code":
                    conditionFalseGroup = lastQueryResult.Where(articles => string.IsNullOrEmpty(articles.Code))
                                                          .OrderBy(articles => articles.Code);

                    conditionTrueGroup = lastQueryResult.Where(articles => !string.IsNullOrEmpty(articles.Code))
                                                        .GroupBy(articles => articles.Code).OrderBy(articles => articles.Key);
                    break;

                case "English":
                    condition = c => !char.IsLetter(c);
                    conditionFalseGroup = lastQueryResult.Where(articles => string.IsNullOrEmpty(articles.English))
                                                          .OrderBy(articles => articles.English);

                    conditionTrueGroup = lastQueryResult.Where(articles => !string.IsNullOrEmpty(articles.English))
                                                        .GroupBy(articles => articles.English.ConditionalTrim(condition)[0].ToString().ToUpper())
                                                        .OrderBy(articles => articles.Key);
                    break;

                case "Russian":
                    condition = c => !char.IsLetter(c);
                    conditionFalseGroup = lastQueryResult.Where(articles => string.IsNullOrEmpty(articles.Russian))
                                                          .OrderBy(articles => articles.Russian);

                    conditionTrueGroup = lastQueryResult.Where(articles => !string.IsNullOrEmpty(articles.Russian))
                                                         .GroupBy(articles => articles.Russian.ConditionalTrim(condition)[0].ToString().ToUpper())
                                                         .OrderBy(articles => articles.Key);
                    break;

                case "Reading":
                    condition = c => !char.IsLetter(c);
                    conditionFalseGroup = lastQueryResult.Where(articles => string.IsNullOrEmpty(articles.Reading))
                                                          .OrderBy(articles => articles.Reading);

                    conditionTrueGroup = lastQueryResult.Where(articles => !string.IsNullOrEmpty(articles.Reading))
                                                        .GroupBy(articles => articles.Reading.ConditionalTrim(condition)[0].ToString().ToUpper())
                                                        .OrderBy(articles => (int)articles.Key[0]);
                    break;

                default:
                    MessageBox.Show("Sort error!");
                    break;
            }

            if (conditionTrueGroup != null)
                foreach (var grp in conditionTrueGroup)
                {
                    output.RenderHeader(grp.Key);

                    foreach (Article article in grp)
                        output.ProcessHtml(article, lastSearchString);
                }

            if (conditionFalseGroup != null && conditionFalseGroup.ToList().Count > 0)
            {
                output.RenderHeader("(N/A)");

                foreach (Article article in conditionFalseGroup)
                    output.ProcessHtml(article, lastSearchString);
            }

            return new OutputResult { OutputString = output.ToString(), OutputCounter = output.ArticlesDisplayed };
        }

        private static bool HasMatches(Article item, string searchString)
        {
            return (item.Kanji != null && item.Kanji.Contains(searchString)) ||
                   (item.Reading != null && item.Reading.Contains(searchString.ToLower())) ||
                   (item.Russian != null && item.Russian.ToLower().Contains(searchString.ToLower())) ||
                   (item.English != null && item.English.ToLower().Contains(searchString.ToLower())) ||
                   (item.Comment != null && item.Comment.ToLower().Contains(searchString.ToLower()));
        }

        public string GetReadingForKanji(string kanji)
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();

            StringBuilder furigana = new StringBuilder();

            // Искать внутри словаря
            foreach (Article article in UniversalDictionary.ListArticles.Where(article => article.Kanji == kanji.Trim()))
                furigana.AppendLine(string.Format("{0}  -  {1} ({2}, {3})", article.Kanji, article.Reading, article.SourceDictionary, article.Code != "" ? article.Code : "..."));

            // если мы не нашли фуригану внутри словаря
            if (furigana.ToString() == "")
            {
                // если файл стороннего словаря присутствует в нужной папке
                if (DictionaryExists())
                {
                    // используем Mecab
                    IEnumerable<KeyValuePair<string, string>> result = MeCab.GetReadings(kanji);

                    foreach (var element in result)
                        furigana.AppendLine(string.Format("{0}  -  {1} (Mecab)", element.Key, element.Value));
                }

                else
                    furigana.AppendLine("Чтение не найдено.");
            }

            Application.UseWaitCursor = false;
            return furigana.ToString();
        }

        private bool DictionaryExists()
        {
            return File.Exists(Path.Combine(Application.StartupPath, "dic\\ipadic\\char.bin"));
        }
    }
}
