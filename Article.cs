﻿namespace TeruSoft
{
    public class Article
    {
        //コード   日本語   読み方   ロシア語   英語   コメント
        string code, addedBy, kanji, reading, russian, english, comment, imagePath;

        // ######## C O N S T R U C T O R ############# 
        public Article(DictionaryType sourceDictionary)
        {
            SourceDictionary = sourceDictionary;
        }

        public DictionaryType SourceDictionary { get; set; }

        public string AddedBy
        {
            get { return addedBy != null ? addedBy.Trim() : ""; }
            set { addedBy = value; }
        }

        public string Kanji
        {
            get { return kanji != null ? kanji.Trim() : ""; }
            set { kanji = value; }
        }

        public string Reading
        {
            get { return reading != null ? reading.Trim() : ""; }
            set { reading = value; }
        }

        public string English
        {
            get { return english != null ? english.Trim() : ""; }
            set { english = value; }
        }
        
        public string Russian
        {
            get { return russian != null ? russian.Trim() : ""; }
            set { russian = value; }
        }
        public string Code
        {
            get { return code != null ? code.Trim() : ""; }
            set { code = value; }
        }
        
        public string ImagePath
        {
            get { return imagePath != null ? imagePath.Trim() : ""; }
            set { imagePath = value; }
        }

        public string Comment
        {
            get { return comment != null ? comment.Trim() : ""; }
            set { comment = value; }
        }
                
        public override string ToString()
        {
            return string.Format("{0} ({1}), {2}, {3}. ({4}) {5} (From {6}, {7})", 
                                 Kanji, Reading, English, Russian, ImagePath, Comment, Code, SourceDictionary);
        }
    }
}
