# Translator's Helpmate

![Screen](screen.png)

## Description

Under the hood it is a wrapper for Excel-based dictionaries.
To run, it needs to have **共有辞書(下書き).xls** and **共有辞書(完成版).xls** files one folder up from where the executable is.

## Interface

Contents of these two files go into three tabs: **Kanseiban**, **Shitagaki** and **Canteen**.

- **Kanseiban** (чистовик) tab holds the contents of the **共有辞書(完成版).xls** file, which is for articles that were checked and approved by one of the senior translators.
- **Shitagaki** (черновик) tab holds the contents of the **共有辞書(下書き).xls** file, which is for articles that are yet to be checked. 
- **Canteen** (столовая) tab holds names of the dishes, served at the canteen.

## Usage

- Select one of the three tabs. **Kanseiban** is the default one.
- Type a string to search for into the search field. Input can be in any of the three languages (Japanese, English, Russian)
- Apply filters by checking subject fields to narrow down the search if needed.
- Press "Search" button.
- Press "Show" button with some subject field filters applied to list all of the articles within the corresponding subject fields.
- Press "Clear" to uncheck all of the filters.
- Press one of the four sort buttons (Код, 読み, Eng, Рус) to sort output by subject field, Japanese reading, English or Russian translation.

## Features

- Has a checkbox to keep on top of other windows.
- Allows to search by kanji, Japanese reading, English or Russian translation depending on user input inside the search field.
- Search results can be filtered by subject field to narrow down the search.
- Search results can be sorted by subject field, Japanese reading, English or Russian translation.
- Allows to search for furigana (reading) of selected kanji (Useful when trying to read some kanji that one can come across in the comments section).
- Allows to search for selected kanji online (Kotobank.jp, Wikipedia.org, Jisho.org etc.).

## Dependencies

[HtmlRenderer](https://github.com/ArthurHub/HTML-Renderer) - managed code library used for HTML output.

[NPOI](https://github.com/tonyqus/npoi) - .NET library used for reading Excel-files.

[NMeCab](https://osdn.net/projects/nmecab/releases/63529) - Japanese morphological analyzer used to read Japanese kanji if those readings were not found within the existing dictionary.

## Builds

[Download section](https://bitbucket.org/valendu/translators-helpmate/downloads/) has two builds:

- Translator's Helpmate Full (26 MB),
- Translator's Helpmate Lite (1 MB), which has all the functionality of the full version, but lacks 'Images' folder and a separate dictionary. Therefore, searching for some kanji readings and clicking on image links will result in corresponding warnings.