﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using TeruSoft.Utilities;

namespace TeruSoft
{
    /// <summary>
    /// Class to provide html body output
    /// </summary>
    public class Output
    {
        private readonly StringBuilder _stringBuilder = new StringBuilder();
        public int ArticlesDisplayed { get; private set; }
        
        public void RenderHeader(string inputString)
        {
        	StringWriter stringWriter = new StringWriter();
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter))
            {
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "header");
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div); 
                htmlWriter.Write(inputString);
                htmlWriter.RenderEndTag();   
            }
            
            _stringBuilder.Append(stringWriter);
        }
        
        public int ProcessHtml(Article article, string searchString)
        {
            ArticlesDisplayed += 1;

            StringWriter stringWriter = new StringWriter();
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter))
            {
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "container");
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);    

                htmlWriter.WriteWithTags(HtmlTextWriterTag.Span, ArticlesDisplayed + ")", HtmlTextWriterAttribute.Class, "counter");                      

                foreach (PropertyInfo p in article.GetType().GetProperties()
                	.Where(p => p.Name == "Kanji" || p.Name == "Code"|| p.Name == "Reading" || p.Name == "English" || p.Name == "Russian" || p.Name == "Comment" || p.Name == "ImagePath")
                    .OrderBy(p => p.MetadataToken))
                {
                    // get value for the current property using reflection
                    string propertyValue = p.GetValue(article, null).ToString();
                    if (propertyValue.Trim() == "") continue;

                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, p.Name.ToLower());
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);                                                        

                    // First character to uppercase
                    propertyValue = propertyValue.FirstCharToUpper();

                    // COMMENTS
                    if (p.Name == "Comment")
                    {
                        if (searchString != default(string))
                            propertyValue = HighlightMatches(propertyValue, searchString);

                        // matches "<span class=" followed by one and more alphanumeric characters followed by "> OR "</span>"
                        // OR one and more alphanumeric characters OR one and more NON-alphanumeric characters
                        // TODO: считает японский и другие языки одним целым, если они не разделены пробелом или любым другим знаком (ねじれ)
                        string pattern = "<span class=\"\\w+\">|</span>|\\w+|\\W+";
                        MatchCollection words = Regex.Matches(propertyValue, pattern);

                        foreach (var word in words)
                            if (Regex.IsMatch(word.ToString(), Patterns.AnyJapaneseCharacters))
                                htmlWriter.WriteWithTags(HtmlTextWriterTag.Span, word.ToString(),
                                    HtmlTextWriterAttribute.Class, "japanese");
                            else
                                htmlWriter.Write(word);
                    }

                    // IMAGE PATH
                    else if (p.Name == "ImagePath")
                        htmlWriter.WriteWithTags(HtmlTextWriterTag.A, "(Изображение)", HtmlTextWriterAttribute.Href, propertyValue);

                    // CODE (to prevent highlighting article code)
                    else if (p.Name == "Code")
                    	htmlWriter.Write(propertyValue);
                    
                    // OTHER
                    else
                    {
                        if (searchString != default(string))
                            propertyValue = HighlightMatches(propertyValue, searchString);
                        htmlWriter.Write(propertyValue);
                    }

                    htmlWriter.RenderEndTag();                                                                               
                }
                htmlWriter.RenderEndTag();                                                                                 
            }
            _stringBuilder.Append(stringWriter);
            return ArticlesDisplayed;
        }

        private static string HighlightMatches(string sourceString, string searchString)
        {
        	int n = 0;
            while ((n = sourceString.ToLower().IndexOf(searchString.ToLower(), n)) != -1)
            {
                string beginTag = "<span class=\"match\">";
                string endTag = "</span>";
                string temp1 = sourceString.Insert(n, beginTag);
                string temp2 = temp1.Insert(n + beginTag.Length + searchString.Length, endTag);

                sourceString = temp2;

                n += beginTag.Length + endTag.Length + 1;
            }

            return sourceString;
        }

        public void Clear()
        {
            _stringBuilder.Length = 0;
            ArticlesDisplayed = 0;
        }

        public override string ToString()
        {
            return _stringBuilder.ToString();
        }
    }
}
