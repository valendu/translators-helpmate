﻿namespace TeruSoft
{
    partial class DictionaryTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.components = new System.ComponentModel.Container();
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DictionaryTab));
        	this.listViewCodes = new System.Windows.Forms.ListView();
        	this.buttonUncheckAll = new System.Windows.Forms.Button();
        	this.buttonShowArticles = new System.Windows.Forms.Button();
        	this.textBoxWarning = new System.Windows.Forms.TextBox();
        	this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
        	this.furiganaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        	this.searchOnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.jishoorgToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.containerPanel = new System.Windows.Forms.Panel();
        	this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
        	this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        	this.исходныйКодToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStrip1 = new System.Windows.Forms.ToolStrip();
        	this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
        	this.toolStripButtonSortByCode = new System.Windows.Forms.ToolStripButton();
        	this.toolStripButtonSortByReading = new System.Windows.Forms.ToolStripButton();
        	this.toolStripButtonSortByEnglish = new System.Windows.Forms.ToolStripButton();
        	this.toolStripButtonSortByRussian = new System.Windows.Forms.ToolStripButton();
        	this.contextMenuStrip1.SuspendLayout();
        	this.contextMenuStrip2.SuspendLayout();
        	this.toolStrip1.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// listViewCodes
        	// 
        	this.listViewCodes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.listViewCodes.CheckBoxes = true;
        	this.listViewCodes.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.listViewCodes.Location = new System.Drawing.Point(4, 3);
        	this.listViewCodes.Name = "listViewCodes";
        	this.listViewCodes.Size = new System.Drawing.Size(806, 82);
        	this.listViewCodes.Sorting = System.Windows.Forms.SortOrder.Ascending;
        	this.listViewCodes.TabIndex = 9;
        	this.listViewCodes.UseCompatibleStateImageBehavior = false;
        	this.listViewCodes.View = System.Windows.Forms.View.List;
        	this.listViewCodes.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listViewCodes_ItemChecked);
        	// 
        	// buttonUncheckAll
        	// 
        	this.buttonUncheckAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.buttonUncheckAll.Enabled = false;
        	this.buttonUncheckAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
        	this.buttonUncheckAll.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.buttonUncheckAll.ForeColor = System.Drawing.Color.Black;
        	this.buttonUncheckAll.Location = new System.Drawing.Point(816, 56);
        	this.buttonUncheckAll.Name = "buttonUncheckAll";
        	this.buttonUncheckAll.Size = new System.Drawing.Size(96, 29);
        	this.buttonUncheckAll.TabIndex = 10;
        	this.buttonUncheckAll.Text = "Clear";
        	this.buttonUncheckAll.UseVisualStyleBackColor = true;
        	this.buttonUncheckAll.Click += new System.EventHandler(this.buttonUncheckAll_Click);
        	// 
        	// buttonShowArticles
        	// 
        	this.buttonShowArticles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.buttonShowArticles.Enabled = false;
        	this.buttonShowArticles.FlatStyle = System.Windows.Forms.FlatStyle.System;
        	this.buttonShowArticles.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.buttonShowArticles.ForeColor = System.Drawing.Color.Black;
        	this.buttonShowArticles.Location = new System.Drawing.Point(816, 3);
        	this.buttonShowArticles.Name = "buttonShowArticles";
        	this.buttonShowArticles.Size = new System.Drawing.Size(96, 29);
        	this.buttonShowArticles.TabIndex = 11;
        	this.buttonShowArticles.Text = "Show";
        	this.buttonShowArticles.UseVisualStyleBackColor = true;
        	this.buttonShowArticles.Click += new System.EventHandler(this.buttonShowArticles_Click);
        	// 
        	// textBoxWarning
        	// 
        	this.textBoxWarning.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.textBoxWarning.BackColor = System.Drawing.Color.Crimson;
        	this.textBoxWarning.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.textBoxWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        	this.textBoxWarning.ForeColor = System.Drawing.Color.White;
        	this.textBoxWarning.Location = new System.Drawing.Point(4, 91);
        	this.textBoxWarning.Multiline = true;
        	this.textBoxWarning.Name = "textBoxWarning";
        	this.textBoxWarning.ReadOnly = true;
        	this.textBoxWarning.Size = new System.Drawing.Size(908, 52);
        	this.textBoxWarning.TabIndex = 15;
        	this.textBoxWarning.Text = "Внимание!\r\nСловарные статьи в данном разделе еще не прошли проверку и могут содер" +
        	"жать как неточности, так и ошибки.\r\nИспользуйте их на свой страх и риск.";
        	this.textBoxWarning.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        	// 
        	// contextMenuStrip1
        	// 
        	this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.furiganaToolStripMenuItem,
        	        	        	this.toolStripSeparator1,
        	        	        	this.searchOnToolStripMenuItem});
        	this.contextMenuStrip1.Name = "contextMenuStrip1";
        	this.contextMenuStrip1.ShowImageMargin = false;
        	this.contextMenuStrip1.Size = new System.Drawing.Size(104, 54);
        	this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuStrip1Opening);
        	// 
        	// furiganaToolStripMenuItem
        	// 
        	this.furiganaToolStripMenuItem.Name = "furiganaToolStripMenuItem";
        	this.furiganaToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
        	this.furiganaToolStripMenuItem.Text = "Фуригана";
        	this.furiganaToolStripMenuItem.Click += new System.EventHandler(this.MenuFuriganaClick);
        	// 
        	// toolStripSeparator1
        	// 
        	this.toolStripSeparator1.Name = "toolStripSeparator1";
        	this.toolStripSeparator1.Size = new System.Drawing.Size(100, 6);
        	// 
        	// searchOnToolStripMenuItem
        	// 
        	this.searchOnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.jishoorgToolStripMenuItem});
        	this.searchOnToolStripMenuItem.Name = "searchOnToolStripMenuItem";
        	this.searchOnToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
        	this.searchOnToolStripMenuItem.Text = "Искать на";
        	// 
        	// jishoorgToolStripMenuItem
        	// 
        	this.jishoorgToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("jishoorgToolStripMenuItem.Image")));
        	this.jishoorgToolStripMenuItem.Name = "jishoorgToolStripMenuItem";
        	this.jishoorgToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
        	this.jishoorgToolStripMenuItem.Text = "Jisho.org";
        	this.jishoorgToolStripMenuItem.Click += new System.EventHandler(this.JishoorgToolStripMenuItemClick);
        	// 
        	// containerPanel
        	// 
        	this.containerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.containerPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	this.containerPanel.Location = new System.Drawing.Point(4, 149);
        	this.containerPanel.Name = "containerPanel";
        	this.containerPanel.Size = new System.Drawing.Size(908, 304);
        	this.containerPanel.TabIndex = 18;
        	// 
        	// contextMenuStrip2
        	// 
        	this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.toolStripMenuItem1,
        	        	        	this.toolStripSeparator2,
        	        	        	this.исходныйКодToolStripMenuItem,
        	        	        	this.оПрограммеToolStripMenuItem});
        	this.contextMenuStrip2.Name = "contextMenuStrip2";
        	this.contextMenuStrip2.ShowImageMargin = false;
        	this.contextMenuStrip2.Size = new System.Drawing.Size(141, 76);
        	// 
        	// toolStripMenuItem1
        	// 
        	this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.toolStripMenuItem2,
        	        	        	this.toolStripMenuItem3,
        	        	        	this.toolStripMenuItem4,
        	        	        	this.toolStripMenuItem5});
        	this.toolStripMenuItem1.Name = "toolStripMenuItem1";
        	this.toolStripMenuItem1.Size = new System.Drawing.Size(140, 22);
        	this.toolStripMenuItem1.Text = "Сортировать по:";
        	// 
        	// toolStripMenuItem2
        	// 
        	this.toolStripMenuItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.toolStripMenuItem2.Name = "toolStripMenuItem2";
        	this.toolStripMenuItem2.Size = new System.Drawing.Size(217, 22);
        	this.toolStripMenuItem2.Text = "Коду";
        	// 
        	// toolStripMenuItem3
        	// 
        	this.toolStripMenuItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.toolStripMenuItem3.Name = "toolStripMenuItem3";
        	this.toolStripMenuItem3.Size = new System.Drawing.Size(217, 22);
        	this.toolStripMenuItem3.Text = "Чтению";
        	// 
        	// toolStripMenuItem4
        	// 
        	this.toolStripMenuItem4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.toolStripMenuItem4.Name = "toolStripMenuItem4";
        	this.toolStripMenuItem4.Size = new System.Drawing.Size(217, 22);
        	this.toolStripMenuItem4.Text = "Значению на английском";
        	// 
        	// toolStripMenuItem5
        	// 
        	this.toolStripMenuItem5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.toolStripMenuItem5.Name = "toolStripMenuItem5";
        	this.toolStripMenuItem5.Size = new System.Drawing.Size(217, 22);
        	this.toolStripMenuItem5.Text = "Значению на русском";
        	// 
        	// toolStripSeparator2
        	// 
        	this.toolStripSeparator2.Name = "toolStripSeparator2";
        	this.toolStripSeparator2.Size = new System.Drawing.Size(137, 6);
        	// 
        	// исходныйКодToolStripMenuItem
        	// 
        	this.исходныйКодToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.исходныйКодToolStripMenuItem.Name = "исходныйКодToolStripMenuItem";
        	this.исходныйКодToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
        	this.исходныйКодToolStripMenuItem.Text = "Исходный код";
        	// 
        	// оПрограммеToolStripMenuItem
        	// 
        	this.оПрограммеToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
        	this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
        	this.оПрограммеToolStripMenuItem.Text = "О программе";
        	// 
        	// toolStrip1
        	// 
        	this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
        	this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.toolStripLabel1,
        	        	        	this.toolStripButtonSortByCode,
        	        	        	this.toolStripButtonSortByReading,
        	        	        	this.toolStripButtonSortByEnglish,
        	        	        	this.toolStripButtonSortByRussian});
        	this.toolStrip1.Location = new System.Drawing.Point(0, 456);
        	this.toolStrip1.Name = "toolStrip1";
        	this.toolStrip1.Size = new System.Drawing.Size(915, 25);
        	this.toolStrip1.TabIndex = 19;
        	this.toolStrip1.Text = "toolStrip1";
        	// 
        	// toolStripLabel1
        	// 
        	this.toolStripLabel1.Name = "toolStripLabel1";
        	this.toolStripLabel1.Size = new System.Drawing.Size(76, 22);
        	this.toolStripLabel1.Text = "Сортировка:";
        	// 
        	// toolStripButtonSortByCode
        	// 
        	this.toolStripButtonSortByCode.AutoSize = false;
        	this.toolStripButtonSortByCode.BackColor = System.Drawing.Color.Maroon;
        	this.toolStripButtonSortByCode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.toolStripButtonSortByCode.ForeColor = System.Drawing.Color.White;
        	this.toolStripButtonSortByCode.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSortByCode.Image")));
        	this.toolStripButtonSortByCode.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripButtonSortByCode.Name = "toolStripButtonSortByCode";
        	this.toolStripButtonSortByCode.Size = new System.Drawing.Size(35, 22);
        	this.toolStripButtonSortByCode.Text = "Код";
        	this.toolStripButtonSortByCode.ToolTipText = "Сортировать по коду области употребления";
        	this.toolStripButtonSortByCode.Click += new System.EventHandler(this.ToolStripButtonSortByCodeClick);
        	// 
        	// toolStripButtonSortByReading
        	// 
        	this.toolStripButtonSortByReading.AutoSize = false;
        	this.toolStripButtonSortByReading.BackColor = System.Drawing.Color.MidnightBlue;
        	this.toolStripButtonSortByReading.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.toolStripButtonSortByReading.ForeColor = System.Drawing.Color.White;
        	this.toolStripButtonSortByReading.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSortByReading.Image")));
        	this.toolStripButtonSortByReading.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripButtonSortByReading.Name = "toolStripButtonSortByReading";
        	this.toolStripButtonSortByReading.Size = new System.Drawing.Size(35, 22);
        	this.toolStripButtonSortByReading.Text = "読み";
        	this.toolStripButtonSortByReading.ToolTipText = "Сортировать по японскому чтению";
        	this.toolStripButtonSortByReading.Click += new System.EventHandler(this.ToolStripButtonSortByReadingClick);
        	// 
        	// toolStripButtonSortByEnglish
        	// 
        	this.toolStripButtonSortByEnglish.AutoSize = false;
        	this.toolStripButtonSortByEnglish.BackColor = System.Drawing.Color.MediumSeaGreen;
        	this.toolStripButtonSortByEnglish.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.toolStripButtonSortByEnglish.ForeColor = System.Drawing.Color.White;
        	this.toolStripButtonSortByEnglish.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSortByEnglish.Image")));
        	this.toolStripButtonSortByEnglish.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripButtonSortByEnglish.Name = "toolStripButtonSortByEnglish";
        	this.toolStripButtonSortByEnglish.Size = new System.Drawing.Size(35, 22);
        	this.toolStripButtonSortByEnglish.Text = "Eng";
        	this.toolStripButtonSortByEnglish.ToolTipText = "Сортировать в алфавитном порядке по английскому значению";
        	this.toolStripButtonSortByEnglish.Click += new System.EventHandler(this.ToolStripButtonSortByEnglishClick);
        	// 
        	// toolStripButtonSortByRussian
        	// 
        	this.toolStripButtonSortByRussian.AutoSize = false;
        	this.toolStripButtonSortByRussian.BackColor = System.Drawing.Color.Black;
        	this.toolStripButtonSortByRussian.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
        	this.toolStripButtonSortByRussian.ForeColor = System.Drawing.Color.White;
        	this.toolStripButtonSortByRussian.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSortByRussian.Image")));
        	this.toolStripButtonSortByRussian.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripButtonSortByRussian.Name = "toolStripButtonSortByRussian";
        	this.toolStripButtonSortByRussian.Size = new System.Drawing.Size(35, 22);
        	this.toolStripButtonSortByRussian.Text = "Рус";
        	this.toolStripButtonSortByRussian.ToolTipText = "Сортировать в алфавитном порядке по русскому значению";
        	this.toolStripButtonSortByRussian.Click += new System.EventHandler(this.ToolStripButtonSortByRussianClick);
        	// 
        	// DictionaryTab
        	// 
        	this.Controls.Add(this.toolStrip1);
        	this.Controls.Add(this.containerPanel);
        	this.Controls.Add(this.textBoxWarning);
        	this.Controls.Add(this.listViewCodes);
        	this.Controls.Add(this.buttonUncheckAll);
        	this.Controls.Add(this.buttonShowArticles);
        	this.Name = "DictionaryTab";
        	this.Size = new System.Drawing.Size(915, 481);
        	this.contextMenuStrip1.ResumeLayout(false);
        	this.contextMenuStrip2.ResumeLayout(false);
        	this.toolStrip1.ResumeLayout(false);
        	this.toolStrip1.PerformLayout();
        	this.ResumeLayout(false);
        	this.PerformLayout();
        }
        private System.Windows.Forms.ToolStripButton toolStripButtonSortByReading;
        private System.Windows.Forms.ToolStripButton toolStripButtonSortByEnglish;
        private System.Windows.Forms.ToolStripButton toolStripButtonSortByRussian;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButtonSortByCode;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel containerPanel;
        private System.Windows.Forms.ToolStripMenuItem jishoorgToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchOnToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem furiganaToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        //private TeruSoft.RichTextBoxEx richTextBoxSearchResults;
        #endregion

        private System.Windows.Forms.ListView listViewCodes;
        private System.Windows.Forms.Button buttonUncheckAll;
        private System.Windows.Forms.Button buttonShowArticles;
        private System.Windows.Forms.TextBox textBoxWarning;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem исходныйКодToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}
